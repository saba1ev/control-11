import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import {registerUser} from "../../store/actions/userActions";

class Register extends Component {
  state = {
    username: '',
    password: '',
    name: '',
    number: '',
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.registerUser({...this.state});
  };

  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
  };

  render() {
    return (
      <Fragment>
        <h2>Register new user</h2>
        {this.props.error && this.props.error.global && (
          <Alert color="danger">
            {this.props.error.global}
          </Alert>
        )}

        <Form onSubmit={this.submitFormHandler}>

          <FormElement
            propertyName="username"
            type="text"
            title="Username"
            value={this.state.username}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('username')}
            autoComplete="new-username"
            placeholder="Enter your desired username"
          />


          <FormElement
            propertyName="password"
            type="password"
            title="Password"
            value={this.state.password}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('password')}
            autoComplete="new-password"
            placeholder="Enter new secure password"
          />
          <FormElement
            propertyName="name"
            type="text"
            title="Name"
            value={this.state.name}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('name')}
            autoComplete="new-name"
            placeholder="Enter your name"
          />
          <FormElement
            propertyName="number"
            type="text"
            title="Phone Number"
            value={this.state.number}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('number')}
            autoComplete="new-number"
            placeholder="Enter your phone number"
          />

          <FormGroup row>
            <Col sm={{offset: 2, size: 10}}>
              <Button type="submit" color="primary">
                Register
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
