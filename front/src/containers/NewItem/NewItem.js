import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {createItem} from "../../store/actions/itemsActions";
import PostItem from "../../components/PostItem/PostItem";
import {fetchCategories} from "../../store/actions/categoryActions";

class CreatePost extends Component {

  createPost = itemData => {
    this.props.itemCreated(itemData).then(() => {
      this.props.history.push('/');
    });

  };

  componentDidMount() {
    this.props.fetchCategories();
    if (!this.props.user) {
      this.props.history.push('/login')
    }
  }

  render() {
    return (
      <Fragment>
        <h2>New post</h2>
        <PostItem onSubmit={this.createPost} categories={this.props.category}/>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  category: state.category.category
});

const mapDispatchToProps = dispatch => ({
  itemCreated: itemData => dispatch(createItem(itemData)),
  fetchCategories: () => dispatch(fetchCategories())
});


export default connect(mapStateToProps, mapDispatchToProps)(CreatePost);

