import React, {Component, Fragment} from 'react';
import {fetchCategories} from "../../store/actions/categoryActions";
import {connect} from "react-redux";
import './Sitebar.css'
import {fetchItems, getItemsById} from "../../store/actions/itemsActions";
import TehThumbnile from "../../components/TehThumbnile/TehThumbnile";
import {NavLink} from "reactstrap";
import {NavLink as RouterLink} from 'react-router-dom'

class Sitebar extends Component {
  componentDidMount(){
    this.props.fetchCategory();
    this.props.fetchItems();
  }
  render() {
    return (
      <Fragment>
        <div className="category">
          <p onClick={this.props.fetchItems}>All category</p>
          {this.props.category.map(item=>{
            return(
              <p key={item._id} onClick={()=>this.props.getItemByCategory(item._id)}>{item.title}</p>
            )
          })}
        </div>
        <div className="items">
          {this.props.items.map(item=>{
            return(
              <div className='wind' key={item._id}>
                <TehThumbnile image={item.image}/>
                <h4>{item.title}</h4>
                <p>{item.price} Сом</p>
                <strong>{item.user.name}</strong>
                <p>{item.user.number}</p>
                <NavLink tag={RouterLink} to={`/item/${item._id}`}>Show >>></NavLink>
              </div>
            )
          })}
        </div>
      </Fragment>
    );
  }
}
const mapStateToProps = state =>({
  category: state.category.category,
  items: state.items.items
});
const mapDispatchToProps = dispatch => ({
  fetchCategory: () => dispatch(fetchCategories()),
  fetchItems: () => dispatch(fetchItems()),
  getItemByCategory: (id)=>dispatch(getItemsById(id))
});

export default connect(mapStateToProps, mapDispatchToProps) (Sitebar);