import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, CardBody, CardTitle, Row} from "reactstrap";
import TehThumbnile from "../../components/TehThumbnile/TehThumbnile";
import {getItemsById} from "../../store/actions/itemsActions";


class SinglePost extends Component {


  componentDidMount() {
    this.props.oneItemId(this.props.match.params.id);
  }


  render() {
    console.log(this.props.oneItem);
    return (
      <Fragment>
        {this.props.oneItem ? <Row style={{margin: '20px'}} sm={12}>
          <CardTitle><TehThumbnile image={this.props.oneItem.image}/></CardTitle>
          <CardBody>
            <h2>{this.props.oneItem.title}</h2>
            <p>{this.props.oneItem.description}</p>
            <p>Posted by: {this.props.oneItem.user.name}</p>
            <p>Phone: {this.props.oneItem.user.number}</p>
          </CardBody>
        </Row> : null}

        {!this.props.user  ? null : <Button>Delete</Button>}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  oneItem: state.items.oneItem
});

const mapDispatchToProps = dispatch => ({
  oneItemId: (id)=> dispatch(getItemsById(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(SinglePost);