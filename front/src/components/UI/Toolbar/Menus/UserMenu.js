import React from 'react';
import {NavLink as RouterNavLink} from "react-router-dom";
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";

const UserMenu = ({user, logout}) => (
  <UncontrolledDropdown nav inNavbar>
    <DropdownToggle nav caret>
      Hello, {user.username}!
    </DropdownToggle>
    <DropdownMenu right>
      <DropdownItem divider/>
      <DropdownItem tag={RouterNavLink} to="/item/new">
        Add new item
      </DropdownItem>
      <DropdownItem onClick={logout}>
        Logout
      </DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>

);

export default UserMenu