import React from 'react';
import chat from '../../assets/images/image_not_available.png'
import {apiURL} from "../../constants";


const styles = {
  width: '150px',
  height: '150px',
  marginRight: '10px'
};

const ForumThumbnail = props => {
  let image = chat;

  if (props.image) {
    image = apiURL + '/uploads/' + props.image;
  }


  return <img src={image} style={styles} className="img-thumbnail" alt="Post Img"/>
};

export default ForumThumbnail;
