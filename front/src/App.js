import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {logoutUser} from "./store/actions/userActions";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Sitebar from "./containers/Sitebar/Sitebar";
import NewItem from "./containers/NewItem/NewItem";
import SingleItem from "./containers/SingleItem/SingleItem";




class App extends Component {
  render() {
    return (
      <Fragment>
        <NotificationContainer/>
        <header>
          <Toolbar
            user={this.props.user}
            logout={this.props.logoutUser}
          />
        </header>
        <Container style={{mxarginTop: '20px'}}>
          <Switch>
            <Route path="/" exact component={Sitebar}/>

            <Route path='/item/new' component={NewItem}/>
            <Route path='/item/:id' component={SingleItem}/>
            <Route path='/login' component={Login}/>
            <Route path='/register' component={Register}/>



          </Switch>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps,)(App));
