import axios from "../../axios-teh";

export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';

export const fetchPostsSuccess = categories => ({type: FETCH_CATEGORY_SUCCESS, categories});




export const fetchCategories = () => {
  return dispatch => {
    return axios.get('/category').then(
      response => dispatch(fetchPostsSuccess(response.data))
    );
  };
};

