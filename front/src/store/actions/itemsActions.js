import axios from '../../axios-teh';


export const FETCH_ITEM_SUCCESS = "FETCH_ITEM_SUCCESS";
export const ONE_ITEM_SUCCESS = 'ONE_ITEM_SUCCESS';
export const CREATE_ITEM_SUCCESS = "CREATE_ITEM_SUCCESS";


export const fetchItemSuccess = (item) =>{
  return {type: FETCH_ITEM_SUCCESS, item}
};

export const createItemSuccess = () =>({type: CREATE_ITEM_SUCCESS});
export const oneItemSuccess = (item) => ({type: ONE_ITEM_SUCCESS, item});

export const fetchItems = () =>{
  return dispatch =>{
    axios.get('/item').then(response=> dispatch(fetchItemSuccess(response.data)))
  }

};
export const getItemsById = id => {
  return dispatch =>{
    axios.get('/item/'+ id).then(response => dispatch(oneItemSuccess(response.data)))
  }
};

export const createItem = itemData => {
  return (dispatch, getState) => {
    const user = getState().users.user;
    return axios.post('/item', itemData,{headers:{'Token': user.token}}).then(
      (response) => dispatch(createItemSuccess(response.data))
    );
  };
};