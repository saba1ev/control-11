import {FETCH_ITEM_SUCCESS, ONE_ITEM_SUCCESS} from "../actions/itemsActions";


const initialState = {
  items: [],
  oneItem: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ITEM_SUCCESS:
      return {
        ...state,
        items: action.item
      };
    case ONE_ITEM_SUCCESS:
       return{...state, oneItem: action.item};
    default:
      return state;
  }
};

export default reducer;
