import {FETCH_CATEGORY_SUCCESS} from "../actions/categoryActions";


const initialState = {
  category: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORY_SUCCESS:
      return {
        ...state,
        category: action.categories
      };
    default:
      return state;
  }
};

export default reducer;
