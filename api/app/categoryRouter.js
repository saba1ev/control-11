const express = require('express');
const Category = require('../models/Category');

const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    Category.find()
      .then(categories => res.send(categories))
      .catch(() => res.sendStatus(500));
  });

  router.get('/id', (req,res)=>{
    Category.findById(req.params.id)
      .then(category => res.send(category))
  })
  return router
};



module.exports = createRouter;
