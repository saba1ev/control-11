const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const item = require('../models/Item');
const auth = require('../middleware/auth');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = () =>{
  const router = express.Router();

  router.get('/', (req, res) => {
    item.find().populate('category', '_id title').populate('user')
      .then(item => res.send(item))
      .catch(() => res.sendStatus(500));
  });

  router.get('/:id', (req, res) => {
    item.findById(req.params.id).populate('user').populate('category')
      .then(item => {
        if (item) res.send(item);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });


  router.delete('/:id', (req, res) => {
    const items = item.find().populate('user');
    item.findById(req.params.id)
      .then(item => {
        if (items === item.user._id){
          res.send(item)
        } else res.sendStatus(403)
      })
  });

  router.post('/', auth,upload.single('image'), (req, res) => {
    const productData = {...req.body, user: req.user._id};

    if (req.file) {
      productData.image = req.file.filename;
    }

    const items = new item(productData);


    items.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });
  return router
};


module.exports = createRouter;