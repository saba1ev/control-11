const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('./config');

const category = require('./app/categoryRouter');
const item = require('./app/itemRouter');
const user = require('./app/userRouter');


const app = express();


app.use(cors());
app.use(express.json());
app.use(express.static('public'));


const port = 8000;


mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/category', category());
  app.use('/item', item());
  app.use('/user', user());



  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  })
});
