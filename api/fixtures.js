const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const User = require('./models/User');
const Category = require('./models/Category');
const Item = require('./models/Item');


const run = async () => {


  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const users = await User.create(
    {
      username: "sobolev",
      password: "sobolev",
      name: "Maksim",
      number: "0556-300-000",
      token: nanoid()
    },
    {
      username: "max",
      password: "1234",
      name: "Ali",
      number: "0551-011-222",
      token: nanoid()
    }
  );

  const [phone, laptop, watch] = await Category.create(
    {
      title: 'Phone',
    },
    {
      title: 'Laptop',
    },
    {
      title: 'Watches',
    },
  );

  await Item.create(
    {
      title: 'Mi a2',
      price: 15000,
      user: users[0]._id,
      category: phone._id,
      image: "a2.png",
      description: 'Продаю телефон Mi a2'
    },
    {
      title: 'ThinkPad',
      price: 100000,
      user: users[1]._id,
      category: laptop._id,
      image: "think.jpeg",
      description: 'Продаю ноутбук ThinkPad'
    },
    {
      title: 'Rolex',
      price: 10000000,
      user: users[0]._id,
      category: watch._id,
      image: "rol.jpeg",
      description: 'Продаю часы Ролекс'
    },
  );

  return connection.close();
};

run().catch(error => {
  console.error('Something went wrong!', error);
});